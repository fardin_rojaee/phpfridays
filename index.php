<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<?php
include 'space.php';
?>

<div class="text-center">

    <h1>echo</h1>

    <?php

    echo "Hello World!";

    ?>
</div>

<div>
    <h1>constans</h1>
    <?php

    define("companyName", "PSP", true);

    echo companyName;
    ?>
</div>


<div>
    <h1>function</h1>

    <?php

    function fullName($name = "micky", $family = "mouse")
    {

        echo "my name is: $name " . " " . "and my family is: $family ";
    }

    fullName("fardin", "rojae");
    ?>
</div>

<div>
    <h1>array pointer</h1>
    <?php

    $transport = array('ali', 'reza', 'mahmood', 'shayan');
    $name = current($transport);
    $next = next($transport);
    $prev = prev($transport);
    $end = end($transport);
    $reset = reset($transport);

    foreach ($transport as $tras) {
        echo " " . $tras . ",";
    }
    space();
    echo "current is :" . " " . $name . "<br>";
    echo "next is:" . " " . $next . "<br>";
    echo "prev is :" . " " . $prev . "<br>";
    echo "end is :" . " " . $end . "<br>";


    ?>


</div>

<div>

    <h1>list of my friend is :</h1>
    <?php

    function myFriends()
    {
        return array("ali", "akbar", "daryoush");
    }

    list($friend1, $friend2, $friend3) = myFriends();

    foreach (myFriends() as $friends) {
        echo $friends . "<br>";
    }


    ?>


</div>

<!--<div>-->
<!---->
<!--    <h1>Link GET</h1>-->
<!---->
<!--    <a href="destenition.php?name=fardin&family=rojaee" title="link">destenition page</a>-->
<!---->
<!--</div>-->

<div>

    <h1>Forms POST</h1>
    <form action="destination.php" method="post">
        <input type="hidden" name="action" value="register">
        <input type="text" name="firstName" placeholder="firstName">
        <input type="text" name="family" placeholder="family">
        <input type="submit" value="submit">

    </form>
    <br>
    <form action="destination.php" method="post">
        <input type="hidden" name="action" value="command">
        <textarea name="massage" id="" cols="30" rows="10"></textarea>
        <input type="submit" value="submit">

    </form>


</div>
<div>
    <h1>Array To Query</h1>
    <?php

    $data = array(
        "name" => "ali",
        "family" => "rezaee",
        "age" => 20
    );

    echo http_build_query($data);

    space();


    ?>
</div>


<div>
    <h1>validate ....</h1>
    <form action="validate.php" method="post">
        <input type="hidden" name="action" value="validate">
        <input type="email" name="email" placeholder="enter your email">
        <br><br>

        <input type="text" name="web" placeholder="enter your web">

        <input type="submit" value="send">
    </form>
</div>

<div>
    <h1>json</h1>
    <?php
    $jsonArray = array("name" => "fardin",
        "family" => "rojaee",
        "age" => 20
    );
    $js = json_encode($jsonArray);
    echo $js;
    space();
    $dec = json_decode($js,true);
    var_dump($dec);
    space();
    echo $dec['name'];


    ?>


</div>


</body>
</html>


